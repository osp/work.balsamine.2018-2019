# flatten image
# Gaussian blur
# rgb noise
# levels (pour repasser le fond en blanc et gonfler un peu le trait)
# convert image to grayscale
# spread
# convert image to indexed

#convert -density 300 in/coeur-02.svg -spread 3 out/01.png
#convert -density 300 in/coeur-02.svg -gaussian-blur 3 -spread 3 out/02.png
#convert -density 300 in/coeur-02.svg -gaussian-blur 3 -noise 10 -spread 3 out/03.png
#convert -density 300 in/coeur-02.svg -gaussian-blur 3 -noise 10 -spread 3 -threshold 50% out/04.png
#convert -density 1200 in/coeur-02.svg -spread 10 out/05.png

for i in in/*; do
	filename=$(basename -- "$i")
	extension="${filename##*.}"
	filename="${filename%.*}"
	convert  -background white -alpha remove -flatten -density 1200 $i -gaussian-blur 3 -noise 14  -type Grayscale -spread 5 -gaussian-blur 1 -threshold 50% out/"${filename##*/}".png
done
