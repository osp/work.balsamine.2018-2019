# Notes on making an html email

First thing first : this is a shitty work to do.

I say that because :
  - you can't expect to use full css3 possibilities
  - you can't expect to have your own fonts
  - you can"t expect your design to work exactly the same on all plateforms (hardware & softwares)
  - it's better to design html email with ```<table></table>```
  - your CSS should be inline

## Why using table ?
  Apparently it's the best way to have a design that stay well in multiple platforms. Knowing also that floats, transforms, absolute/relative positionning doesn't work well every where. Table are the safest way to control what your email will look like, even tho it's not the easiest coding method in 2018.

## Notes on tables
  - margins will not work, use padding in your ```<td>``` to position your elements
  - you can nest ```<table>``` inside ```<td>``` of another ```<table>``` if you want to play it hardcore...
  - or you can have ```<td>``` with regular ```<div>``` inside

## Some strange things that happened to me

### Svg & Png stories
Gmail did not wanted .svg type images (Outlook had no problem with them). I had to use .png instead. But the thing is .png with transparency rendered blurry for some reason, putting a background color resolved the problem. But I had some problem with simple white background, for some reason...

### True black
For a strange reason, the text of my email where transformed from regular black to greyish color, on both Outlook & Gmail. ```#000000```, ```#000```, ```rgb(0,0,0)``` didn't work. The problem was solved by simply putting ```color:"black"```; manually on each ```<div>``` with text in it. Leaving empty font color to have the default black didn't work either.
