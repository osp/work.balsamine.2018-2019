
<div class="article ou-suis-je" markdown="true">

#Où suis-je&thinsp;? Qu’ai-je fait&thinsp;?

##de Pauline d’Ollone 

2.11&thinsp;→&thinsp;3.11<br/>
et 5.11&thinsp;<br/>→&thinsp;10.11
{: .dates}

à 20H30<br/>
Création théâtre
{: .informations}

<div class="visuels push-down cover" markdown="true">
![ou-suis-je-01](../../img/ou-suis-je-01.svg){: .all}
![ou-suis-je-01](../../img/ou-suis-je-01-vecto.svg){: .vecto}
![ou-suis-je-01](../../img/ou-suis-je-01-blanc.svg){: .pantone-blanc}
</div>

<div class="visuels" markdown="true">
![logo-balsa](../../img/logo-balsa.svg){: .all}
![logo-balsa](../../img/logo-balsa-vecto.svg){: .vecto}
![logo-balsa](../../img/logo-balsa-couleur.svg){: .pantone-rose}
</div>

Théâtre la Balsamine </br>Avenue Félix Marchal,1 </br>1030 Bruxelles
<br/>Réservation&nbsp;: 02 735 64 68<br/>reservation@balsamine.be<br/>www.balsamine.be
{: .contact}

Les points communs entre Roméo et Juliette, le Journal de Goebbels ou encore un site anti-arnaque&nbsp;? Des person&shy;nages qui cherchent, à n’importe quel prix, un sens à leur destinée. Trois histoires, trois formes d’asservissement volontaire.
{: .figure .push .push-down}


Avec 
:   Marie Bos, Pierange Buondelmonte, Héloïse Jadoul <br/>et Jérémie Siska<br/>

Texte et mise en scène<br/>
:   Pauline d’Ollone<br/>

Scénographie 
:   Pierange Buondelmonte et Guillaume Fromentin<br/>

Création lumière
:   Renaud Ceulemans<br/>

Assistanat à la mise en scène
:   Sarah Messens<br/>

Construction du décor<br/>
:   Didier Rodot<br/>

Production déléguée
:   Théâtre la Balsamine<br/>

En coproduction avec le Théâtre la Balsamine,  L’Ancre–Théâtre Royal et La Coop asbl. Avec les soutiens du Centre des Arts Scéniques, de la Fédération Wallonie-Bruxelles – Aide aux projets théâtraux, de Shelterprod, de taxshelter.be, d’ING et du Tax-shelter du gouvernement fédéral belge.
{.production}


#### Fabriquer un </br>piège à mouche

<div class="visuels verso" markdown="true">
![alt](../../img/ou-suis-je-02.svg){: .all}
![alt](../../img/ou-suis-je-02-vecto.svg){: .vecto}
![alt](../../img/ou-suis-je-02-couleur.svg){: .pantone-rose}
</div>

<div class="visuels verso" markdown="true">
![ou-suis-je-03](../../img/ou-suis-je-03.svg){: .all}
![ou-suis-je-03](../../img/ou-suis-je-03-vecto.svg){: .vecto}
![ou-suis-je-03](../../img/ou-suis-je-03-couleur.svg){: .pantone-rose}
</div>

<div class="visuels verso" markdown="true">
![alt](../../img/ou-suis-je-04.svg){: .all}
![alt](../../img/ou-suis-je-04-vecto.svg){: .vecto}
![alt](../../img/ou-suis-je-04-couleur.svg){: .pantone-rose}
</div>

<div class="visuels verso" markdown="true">
![ou-suis-je-05](../../img/ou-suis-je-05.svg){: .all}
![ou-suis-je-05](../../img/ou-suis-je-05-vecto.svg){: .vecto}
![ou-suis-je-05](../../img/ou-suis-je-05-couleur.svg){: .pantone-rose}
</div>

![logos-all](../../img/logos-all.svg)
{: .figure .push .push-down}

Éditeur responsable : Monica Gomes, Avenue Félix Marchal, 1 - 1030 Bruxelles — Design : OSP
{: .editeur}


</div>
