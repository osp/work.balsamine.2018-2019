<div class="article origine" markdown="true">

#ØRIGINE

##de Silvio Palomo/Le comité des fêtes 

3.10&nbsp;→&nbsp;5.10<br/>
et 9.10&nbsp;<br/>
→&nbsp;13.10
{: .dates}

à 20H30<br/>
Création théâtre
{: .informations}


<div class="visuels push cover" markdown="true">
![origine-1](../../img/origine-01-rose-ours.svg){: .all}
![origine-1](../../img/origine-01-rose-ours-vecto.svg){: .vecto}
![origine-1](../../img/origine-01-blanc-ours.svg){: .pantone-blanc .push-down}
</div>

<div class="visuels" markdown="true">
![logo-balsa](../../img/logo-balsa.svg){: .all}
![logo-balsa](../../img/logo-balsa-vecto.svg){: .vecto}
![logo-balsa](../../img/logo-balsa-couleur.svg){: .pantone-rose}
</div>

Théâtre la Balsamine </br>Avenue Félix Marchal,1 </br>1030 Bruxelles
<br/>Réservation&nbsp;: 02 735 64 68<br/>reservation@balsamine.be<br/>www.balsamine.be
{: .contact}

Les protagonistes de cette aventure sont un homme et une femme. Un pique-nique s’organise. Un ours et un pingouin s’invitent. Du coup, leur évènement s’en trouve bouleversé.
{: .figure .push .push-down}

Avec 
:   Léonard Cornevin, Aurélien&nbsp;Dubreuil-Lachaud, Manon Joannotéguy et Nicole Stankiewicz<br/>

Texte et mise en scène
:   Silvio&nbsp;Palomo<br/>

Scénographie 
:   Itzel Palomo<br/>

Création lumière
:   Léonard Cornevin<br/>

Production déléguée
:   Théâtre la Balsamine<br/>

En coproduction avec le Théâtre la Balsamine et La Coop asbl. Avec les soutiens de la Fédération Wallonie-Bruxelles – Aide aux projets théâtraux, de Shelterprod, de taxshelter.be, d’ING et du Tax-shelter du gouvernement fédéral belge.
{: .production}

#### Déplier une </br>table pliante

<div class="visuels verso" markdown="true">
![origine-2](../../img/origine-02.svg){: .all}
![origine-2](../../img/origine-02-vecto.svg){: .vecto}
![origine-2](../../img/origine-02-couleur.svg){: .pantone-rose}
</div>

<div class="visuels verso" markdown="true">
![origine-3](../../img/origine-03.svg){: .all}
![origine-3](../../img/origine-03-vecto.svg){: .vecto}
![origine-3](../../img/origine-03-couleur.svg){: .pantone-rose}
</div>

<div class="visuels figure demi verso" markdown="true">
![origine-4](../../img/origine-04.svg){: .all}
![origine-4](../../img/origine-04-vecto.svg){: .vecto}
![origine-4](../../img/origine-04-couleur.svg){: .pantone-rose}
</div>

<div class="visuels verso" markdown="true">
![origine-5](../../img/origine-05.svg){: .all}
![origine-5](../../img/origine-05-vecto.svg){: .vecto}
![origine-5](../../img/origine-05-couleur.svg){: .pantone-rose}
</div>

<div class="visuels verso" markdown="true">
![origine-6](../../img/origine-06.svg){: .all}
![origine-6](../../img/origine-06-vecto.svg){: .vecto}
![origine-6](../../img/origine-06-couleur.svg){: .pantone-rose}
</div>

![logos-all](../../img/logos-all.svg)
{: .figure .push .push-down}


Éditeur responsable : Monica Gomes, Avenue Félix Marchal, 1 - 1030 Bruxelles — Design : OSP
{: .editeur}

</div>
