<div class="article valhalla push" markdown="true">

#Valhalla ou le crépuscule des dieux

à 20H30<br/>
Création cirque
{: .informations .push-down}

de Petri Dish / Anna Nilsson et Sara Lemaire
{: .informations}

21.11 → 23.11
{: .dates}



<div class="visuels push-down cover" markdown="true">
![alt](../../img/valhalla-01-vecto02.svg){: .all}
![alt](../../img/valhalla-01-vecto02.svg){: .vecto}
![alt](../../img/valhalla-01-blanc02.svg){: .pantone-rose}
</div>

<div class="visuels" markdown="true">
![logo-balsa](../../img/logo-balsa.svg){: .all}
![logo-balsa](../../img/logo-balsa-vecto.svg){: .vecto}
![logo-balsa](../../img/logo-balsa-couleur.svg){: .pantone-rose}
</div>

Théâtre la Balsamine </br>Avenue Félix Marchal,1 </br>1030 Bruxelles
<br/>Réservation&nbsp;: 02 735 64 68<br/>reservation@balsamine.be<br/>www.balsamine.be
{: .contact}

Le crépuscule, un bateau pris dans la glace. Le naufrage <br/>d'un équipage qui sombre dans la solitude et la folie. Il s'y <br/>joue du cirque, du chant, de la danse et de la cornemuse. <br/>Les esprits s'échauffent. La mutinerie commence !
{: .figure .push .push-down}

Avec 
:   Joris Baltz, Viola Baroncelli, Thomas Dechaufour, Laura Laboureur, Carlo Massari, Anna Nilsson et Jef Stevens<br/>

Concept et mise en scène
:  Anna Nilsson et Sara Lemaire<br/>

Création lumière
:   Thibault Condy<br/>

Technique
:   Tonin Bruneton, <br/>Cristian Gutierrez et Camille Rolovic

Une production de Petri Dish et d’I.S.E. asbl en coproduction avec le Théâtre la Balsamine, le Groupe des 20 Théâtres en Île-de-France, Theater Op De Markt – Dommelhof et le Centre Culturel du Brabant Wallon. Avec l’aide de la Fédération Wallonie-Bruxelles, service du cirque, Kulturradet/Swedish Arts Council <br/>et du Konstnärsnämnden - the Swedish Arts Grants Committee. Avec les soutiens du Circuscentrum, du Théâtre Marni, du Cirkus Cirkör, de Wolubilis, de Latitude 50, de l’Espace Catastrophe - Centre International de Création des Arts du Cirque, de Subtopia et de Dansens Hus.
{.production .push-down}

#### Tenir les voiles <br/>pendant la tempête

<div class="visuels verso" markdown="true">
![alt](../../img/valhalla-02.svg){: .all}
![alt](../../img/valhalla-02-vecto.svg){: .vecto}
![alt](../../img/valhalla-02-couleur.svg){: .pantone-rose}
</div>

<div class="visuels verso" markdown="true">
![alt](../../img/valhalla-03.svg){: .all}
![alt](../../img/valhalla-03-vecto.svg){: .vecto}
![alt](../../img/valhalla-03-couleur.svg){: .pantone-rose}
</div>

<div class="visuels verso" markdown="true">
![alt](../../img/valhalla-04.svg){: .all}
![alt](../../img/valhalla-04-vecto.svg){: .vecto}
![alt](../../img/valhalla-04-couleur.svg){: .pantone-rose}
</div>

![logos-all](../../img/logos-selec.svg)
{: .figure .push .push-down}


Éditeur responsable : Monica Gomes, Avenue Félix Marchal, 1 - 1030 Bruxelles — Design : OSP
{: .editeur}

<!-- <div class="visuels verso" markdown="true">
![](../../img/valhalla8-black.jpg){: .photos}
</div> -->


</div>
