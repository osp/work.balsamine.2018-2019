<div class="article ludum" markdown="true">

#Ludum

## d’Anton Lachky

02.04 <br>→ 05.04
{: .dates}

Création danse
{: .informations}

<div class="visuels cover-ludum push" markdown="true">
![ludum-00](../../img/ludum-00.svg){: .all}
![ludum-00](../../img/ludum-00-vecto.svg){: .vecto}
![ludum-00](../../img/ludum-00-blanc.svg){: .pantone-rose}
</div>

<div class="visuels" markdown="true">
![logo-balsa](../../img/logo-balsa.svg){: .all}
![logo-balsa](../../img/logo-balsa-vecto.svg){: .vecto}
![logo-balsa](../../img/logo-balsa-couleur.svg){: .pantone-rose }
</div>

Théâtre la Balsamine </br>Avenue Félix Marchal,1 </br>1030 Bruxelles
<br/>Réservation : 02 735 64 68<br/>reservation@balsamine.be<br/>www.balsamine.be
{: .contact .push-down}

Les habitants de Ludum présentent tous une particularité, une bizarrerie&nbsp;: ils sont libres d'être et&nbsp;de faire ce qu'ils veulent. À Ludum, société ultra intégrante où les lubies des uns et des autres coexistent en parfaite harmonie, le jeu est loi.
{: .figure .push .push-down}

Avec
:   Dennis Alamanos, Robert Anderson, Guilhem Chantir, Ana Karenina Lambrechts et Ioulia Zacharaki<br/>

Concept et chorégraphie
:   Anton Lachky<br/>

Une production d’Anton Lachky Company en coproduction avec le Théâtre la Balsamine et Charleroi danse.
{.production .push-down}

#### Enfoncer un clou

<div class="visuels verso" markdown="true">
![ludum-01](../../img/ludum-01.svg){: .all}
![ludum-01](../../img/ludum-01-vecto.svg){: .vecto}
![ludum-01](../../img/ludum-01-blanc.svg){: .pantone-rose}
</div>

<div class="visuels verso" markdown="true">
![alt](../../img/ludum-02.svg){: .all}
![alt](../../img/ludum-02-vecto.svg){: .vecto}
![alt](../../img/ludum-02-blanc.svg){: .pantone-rose}
</div>

<div class="visuels verso" markdown="true">
![alt](../../img/ludum-03.svg){: .all}
![alt](../../img/ludum-03-vecto.svg){: .vecto}
![alt](../../img/ludum-03-blanc.svg){: .pantone-rose}
</div>

![logos-all](../../img/logos-all.svg)
{: .figure .push .push-down}

Éditeur responsable : Monica Gomes, Avenue Félix Marchal, 1 - 1030 Bruxelles — Design : OSP
{: .editeur}

</div>
