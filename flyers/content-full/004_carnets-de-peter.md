<div class="article carnets-peter" markdown="true">

#Les Carnets <br/>de Peter

##Théâtre du Tilleul

27.12 → 29.12<br/> et
03.01 → 06.01
{: .dates}

Création<br/>
Spectacle d’ombres et de musique<br/> – dès 7 ans
{: .informations .push-down}

<div class="visuels cover push-down" markdown="true">
![peter](../../img/carnets-00.svg){: .all}
![peter](../../img/carnets-00-vecto.svg){: .vecto}
![peter](../../img/carnets-00-blanc.svg){: .pantone-rose}
</div>

<div class="visuels" markdown="true">
![logo-balsa](../../img/logo-balsa.svg){: .all}
![logo-balsa](../../img/logo-balsa-vecto.svg){: .vecto}
![logo-balsa](../../img/logo-balsa-couleur.svg){: .pantone-rose}
</div>

Théâtre la Balsamine </br>Avenue Félix Marchal,1 </br>1030 Bruxelles
<br/>Réservation&nbsp;: 02 735 64 68<br/>reservation@balsamine.be<br/>www.balsamine.be
{: .contact}

Peter, 87 ans, tente de se souvenir de son enfance : l’Allemagne, Munich 1936, les défilés militaires, les&nbsp;drapeaux, l’émigration en Amérique…
{: .figure .push .push-down}

Avec
:   Carine Ermans, Carlo Ferrante, Sylvain Geoffray et Alain Gilbert<br/>

Conception du spectacle
:  Carine Ermans et Sylvain Geoffray<br/>

Conseil dramaturgique
:   Louis-Dominique Lavigne<br/>

Musique
:   Alain Gilbert<br/>

Mise en scène
:   Sabine Durand<br/>

Lumière
:  Mark Elst<br/>

Régie
:   Thomas Lescart<br/>

Scénographie
:   Pierre-François Limbosch et Alexandre Obolensky<br/>

Peintures
:   Alexandre et Eugénie Obolensky aidés de Malgorzata Dzierzawska et Tancrède de Ghellinck<br/>

Costumes
:   Silvia Hasenclever<br/>

Travail du mouvement
:   Isabelle Lamouline<br/>

Images animées
:  Patrick Theunen et&nbsp;Graphoui<br/>

Accessoires
:   Amalgames<br/>

Production 
:   Mark Elst<br/>

En coproduction avec le Théâtre la Balsamine avec la collaboration du Théâtre La montagne magique, du CDWEJ et de La Maison des Cultures et de la Cohésion Sociale de Molenbeek-Saint-Jean.
{.production}

Le Théâtre du Tilleul est en résidence artistique et administrative au Théâtre la Balsamine.
{.production .push-down}

#### Monter une <br/>bibliothèque

<div class="visuels verso" markdown="true">
![alt](../../img/carnets-01.svg){: .all}
![alt](../../img/carnets-01-vecto.svg){: .vecto}
![alt](../../img/carnets-01-blanc.svg){: .pantone-rose}
</div>

<div class="visuels verso push" markdown="true">
![peter-led](../../img/carnets-02.svg){: .all}
![peter-led](../../img/carnets-02-vecto.svg){: .vecto}
![peter-led](../../img/carnets-02-blanc.svg){: .pantone-rose}
</div>

![logos-all](../../img/logos-all.svg)
{: .figure .push .push-down}


Éditeur responsable : Monica Gomes, Avenue Félix Marchal, 1 - 1030 Bruxelles — Design : OSP
{: .editeur}

</div>
