<div class="article lost" markdown="true">

#Lost in Ballets Russes

##de Lara Barsacq

6.12 → 8.12
{: .dates}

à 20H30<br/>
Danse
{: .informations .push-down}

<div class="visuels cover push-down" markdown="true">
![lost-01](../../img/lost-01-02-vecto.svg){: .all}
![lost-01](../../img/lost-01-02-vecto.svg){: .vecto}
![lost-01](../../img/lost-01-02-blanc.svg){: .pantone-rose}
</div>

<div class="visuels" markdown="true">
![logo-balsa](../../img/logo-balsa.svg){: .all}
![logo-balsa](../../img/logo-balsa-vecto.svg){: .vecto}
![logo-balsa](../../img/logo-balsa-couleur.svg){: .pantone-rose}
</div>

Théâtre la Balsamine </br>Avenue Félix Marchal,1 </br>1030 Bruxelles
<br/>Réservation : 02 735 64 68<br/>reservation@balsamine.be<br/>www.balsamine.be
{: .contact}

Quelques dessins d'un grand-oncle (Léon Bakst-peintre, décorateur et costumier des Ballets russes <br/>au début du XX<sup>e</sup> siècle), une danse en hommage à papa, quelques ustensiles et objets des années 1970. Les chemins de la mémoire génèrent une chorégraphie autobiographique pleine de sincérité.<br/>
{: .figure .push .push-down}

Projet, texte, chorégraphie, dramaturgie et interprétation
:   Lara Barsacq<br/>

Aide à la dramaturgie<br/>
:  Clara Le Picard<br/>

Conseils artistiques<br/>
:   Gaël Santisteva<br/>

Costumes
:   Sofie Durnez<br/>

Création lumière
:   Kurt Lefevre<br/>

Musique
:   Bauhaus, Claude Debussy <br/>et Maurice Ravel<br/>

Participation
:   Lydia Stock Brody <br/>et Nomi Stock Meskin<br/>

Une production asbl Gilbert et Stock, en coproduction avec Charleroi danse. Avec le soutien de la Fédération Wallonie-Bruxelles - service de la danse, de Wallonie-Bruxelles International, de La Bellone, de la Ménagerie de Verre et du Théâtre la Balsamine.
{.production .push .push-down}

#### Entretenir <br/>une fougère

<div class="visuels verso" markdown="true">
![lost-planter-fougere2](../../img/lost-planter-fougere2.svg){: .all}
![lost-planter-fougere2](../../img/lost-planter-fougere2-vecto.svg){: .vecto}
![lost-planter-fougere2](../../img/lost-planter-fougere2-couleur.svg){: .pantone-rose}
</div>

<div class="visuels verso" markdown="true">
![alt](../../img/lost-02.svg){: .all}
![alt](../../img/lost-02-vecto.svg){: .vecto}
![alt](../../img/lost-02-blanc.svg){: .pantone-rose}
</div>

![logos-all](../../img/logos-selec.svg)
{: .figure .push .push-down}


Éditeur responsable : Monica Gomes, Avenue Félix Marchal, 1 - 1030 Bruxelles — Design : OSP
{: .editeur}

</div>
