<div class="article poison" markdown="true">

#Laboratoire POISON

##d’Adeline Rosenstein

29.01 <br/>→ 02.02
{: .dates}

Création <br/> Théâtre documentaire
{: .informations .push-down}


<div class="visuels cover push-down" markdown="true">
![alt](../../img/laboratoire-01.svg){: .all}
![alt](../../img/laboratoire-01-vecto.svg){: .vecto}
![alt](../../img/laboratoire-01-blanc.svg){: .pantone-rose}
</div>

<div class="visuels" markdown="true">
![logo-balsa](../../img/logo-balsa.svg){: .all}
![logo-balsa](../../img/logo-balsa-vecto.svg){: .vecto}
![logo-balsa](../../img/logo-balsa-couleur.svg){: .pantone-rose}
</div>

Théâtre la Balsamine </br>Avenue Félix Marchal,1 </br>1030 Bruxelles
<br/>Réservation&nbsp;: 02 735 64 68<br/>reservation@balsamine.be<br/>www.balsamine.be
{: .contact}

Des documents exhumés par le sociologue Jean-Michel Chaumont, dans le cadre de sa recherche sur les codes de conduite du combattant défait, sont le cœur de cette création théâtrale documentaire. À quel instant commence-t-on à ressentir la haine et à condamner l’autre ?
{: .figure .push .push-down}


Les héros purs ne sont-ils pas condamnés à jouer la comédie ? A quel stade, l’artifice devient-il synonyme de couardise ?
{: .punchline .off}

Avec
:   Brune Bazin, Olindo Bolzan, Ninon Borsei, Léa Drouet, Rémi Faure, Titouan Quitot, Martin Rouet et Thibaut Wenger<br/>

Texte et mise en scène
:   Adeline Rosenstein<br/>

Création son
:   Andrea Neumann<br/>

Scénographie
:   Yvonne Harder<br/>

Création lumière et direction technique
:   Caspar Langhoff<br/>

Costumes
:  Rita Belova<br/>

Production déléguée
:   Leïla Di&nbsp;Gregorio<br/>

Une production Little Big Horn en coproduction avec le Théâtre la Balsamine et la Coop asbl. Avec l’aide de la Fédération Wallonie-Bruxelles – Service du Théâtre et Service de la Promotion des lettres, de la Cocof et du kunstencentrum Buda. Avec le soutien de taxshelter.be, d’ING et du tax-shelter du gouvernement fédéral belge.
{.production .push .push-down}

#### SE FAIRE MAL OU <br/>TORTURER QUELQU’UN

<div class="visuels verso push" markdown="true">
![laboratoire-deux](../../img/laboratoire-02.svg){: .all}
![laboratoire-deux](../../img/laboratoire-02-vecto.svg){: .vecto}
![laboratoire-deux](../../img/laboratoire-02-blanc.svg){: .pantone-rose}
</div>

![logos-all](../../img/logos-all.svg)
{: .figure .push .push-down}


Éditeur responsable : Monica Gomes, Avenue Félix Marchal, 1 - 1030 Bruxelles — Design : OSP
{: .editeur}

</div>
