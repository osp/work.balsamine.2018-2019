<div class="article poison" markdown="true">

#Laboratoire POISON

##d’Adeline Rosenstein

29.01 → 02.02
{: .dates}

à 20H30<br/>
Création<br/>
Théâtre documentaire
{: .informations .push-down}


<div class="visuels cover push-down" markdown="true">
![labo-veines](../../img/laboratoire-01-veines.svg){: .all}
![labo-veines](../../img/laboratoire-01-veines-vecto.svg){: .vecto}
![labo-veines](../../img/laboratoire-01-veines-blanc.svg){: .pantone-rose}
</div>

<div class="visuels" markdown="true">
![logo-balsa](../../img/logo-balsa.svg){: .all}
![logo-balsa](../../img/logo-balsa-vecto.svg){: .vecto}
![logo-balsa](../../img/logo-balsa-couleur.svg){: .pantone-rose}
</div>

Théâtre la Balsamine </br>Avenue Félix Marchal,1 </br>1030 Bruxelles
<br/>Réservation&nbsp;: 02 735 64 68<br/>reservation@balsamine.be<br/>www.balsamine.be
{: .contact}

Des documents exhumés par le sociologue Jean-Michel Chaumont, dans le cadre de sa recherche <br/>sur les codes de conduite du combattant défait, <br/>sont le cœur de cette création théâtrale documentaire. <br/>Entre trahir et se tuer, y a-t-il une autre possibilité ? <br/>Bienvenue au **LABORATOIRE POISON**, <br/>dans les murmures, codes <br/>et actes de résistance.
{: .figure .push .push-down}


Les héros purs ne sont-ils pas condamnés à jouer la comédie ? A quel stade, l’artifice devient-il synonyme de couardise ?
{: .punchline .off}


<div class="visuels verso push-down" markdown="true">
![panneau-labo](../../img/laboratoire-01-vecto.svg){: .all}
![panneau-labo](../../img/laboratoire-01-vecto.svg){: .vecto}
![panneau-labo](../../img/laboratoire-01-vecto.svg){: .pantone-rose}
</div>

Conception, écriture et mise <br/>en scène
:   Adeline Rosenstein<br/>

Assistante à la mise en scène
:   Marie Devroux<br/>

Composition sonore<br/>
:   Andrea Neumann<br/>

Espace
:   Yvonne Harder<br/>

Éclairage et direction technique
:   Caspar Langhoff<br/>

Assistante éclairage
:   Mélodie Polge<br/>

Costumes
:   Rita Belova<br/>

Administration
:   Manon Faure<br/>

Production
:   Leïla<br/>
Di Gregorio<br/>

Avec
:   Brune Bazin, Olindo Bolzan, Ninon Borsei, Léa Drouet, Rémi Faure, Isabelle Nouzha, Titouan Quittot, Martin Rouet et Thibaut Wenger

Une production Little Big Horn asbl en coproduction avec le Théâtre la Balsamine et la Coop asbl.<br/>
Avec les soutiens de la Fédération Wallonie-Bruxelles – Services du Théâtre, de la Promotion des lettres<br/>et de la Cellule DOB, de la Cocof, du Kunstencentrum Buda, de taxshelter.be, d’ING, du tax-shelter<br/>du gouvernement fédéral belge, de Zoo Théâtre, de l’Esact et de Les Bancs Publics – lieu d’expérimentations culturelles.
{.production .push-down}

![logos-all](../../img/logos-laboratoire-poison.svg)
{: .figure .push .push-down}



Éditeur responsable : Monica Gomes, Avenue Félix Marchal, 1 - 1030 Bruxelles — Design : OSP
{: .editeur}

</div>
