<div class="article ludum" markdown="true">

#Ludum

## d’Anton Lachky

02.04 → 05.04
{: .dates}

à 20H30<br/>
Dance show<br/>
In the context <br/>of Brussels, dance! <br/>Focus on contemporary <br/>dance.
{: .informations}

<div class="visuels cover-ludum push push-down" markdown="true">
![ludum-00](../../img/ludum-marteau-00.svg){: .all}
![ludum-00](../../img/ludum-marteau-00-vecto.svg){: .vecto}
![ludum-00](../../img/ludum-marteau-00-blanc.svg){: .pantone-rose}
</div>

<div class="visuels" markdown="true">
![logo-balsa](../../img/logo-balsa.svg){: .all}
![logo-balsa](../../img/logo-balsa-vecto.svg){: .vecto}
![logo-balsa](../../img/logo-balsa-couleur.svg){: .pantone-rose }
</div>

Théâtre la Balsamine </br>Avenue Félix Marchal,1 </br>1030 Bruxelles
<br/>Réservation : 02 735 64 68<br/>
reservation@balsamine.be<br/>
www.balsamine.be
{: .contact .push-down}

À Ludum, société ultra laxiste, les habitants sont libres d'être et de faire ce qu'ils veulent ; leurs lubies coexistent en parfaite harmonie.
Mais si le jeu est loi, les règles sont troubles. S’agit-il de jouer à être ou être vraiment ? Comment faire sens quand la frontière entre la liberté et le chaos semble si ténue ?
{: .figure .push .push-down}

In the super lax society of Ludum, residents are free to be and to do what they want; their whims coexist in perfect harmony.
But if playing is the law, the rules are rather obscure. Is it a question of playing a role or of actually being? What does it mean when the line between freedom and chaos seems so tenuous?
{: .figure .push .push-down .en}

Avec
:   Guilhem Chatir, Lewis Cook, Àngel Duran, Anna Karenina Lambrechts, Hyaejin Lee, Patricia Rotondaro et Ioulia Zacharaki<br/>

Concept et chorégraphie
:   Anton Lachky<br/>

Costumes
:   Britt Angé<br/>

Lumière/ Son
:   Tom Daniels<br/>

Production déléguée
:   Théâtre la Balsamine

Une production d'Anton Lachky Company, en coproduction avec le Théâtre la Balsamine, Charleroi danse et Steps, Festival de Danse du Pour-cent culturel Migros.
Avec le soutien de la Fédération Wallonie-Bruxelles- Service de la danse.
{.production .push-down}

#### Enfoncer un clou

<div class="visuels verso" markdown="true">
![ludum-01](../../img/ludum-01.svg){: .all}
![ludum-01](../../img/ludum-01-vecto.svg){: .vecto}
![ludum-01](../../img/ludum-01-blanc.svg){: .pantone-rose}
</div>

<div class="visuels verso" markdown="true">
![alt](../../img/ludum-02.svg){: .all}
![alt](../../img/ludum-02-vecto.svg){: .vecto}
![alt](../../img/ludum-02-blanc.svg){: .pantone-rose}
</div>

<div class="visuels verso" markdown="true">
![alt](../../img/ludum-03.svg){: .all}
![alt](../../img/ludum-03-vecto.svg){: .vecto}
![alt](../../img/ludum-03-blanc.svg){: .pantone-rose}
</div>

![logos-all](../../img/logos-ludum.svg)
{: .figure .push .push-down}

Éditeur responsable : Monica Gomes, Avenue Félix Marchal, 1 - 1030 Bruxelles — Design : OSP
{: .editeur}

</div>
