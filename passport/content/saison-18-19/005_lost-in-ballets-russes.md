<div class="push">&nbsp;</div>

<div class="article lost" markdown="true">

#Lost in Ballets Russes

##de Lara Barsacq

6.12 → 8.12
{: .dates}

Danse
{: .informations}

<div class="visuels" markdown="true">
![lost-01](../../img/lost-01.svg){: .all}
![lost-01](../../img/lost-01-vecto.svg){: .vecto}
![lost-01](../../img/lost-01-couleur.svg){: .pantone-orange}
</div>

Hommage au père, à nos pairs et aux fougères…
{: .punchline .off}

Quelques dessins d’un grand-oncle (Léon Bakst - peintre, décorateur et costumier des Ballets russes au début du XX<sup>e</sup> siècle), une danse en hommage à papa, quelques ustensiles et objets des années 1970. Les chemins de la mémoire génèrent une&nbsp;chorégraphie autobiographique pleine de&nbsp;sincérité.<br/>
&nbsp;
{: .figure .push}

Projet, texte, chorégraphie, dramaturgie et interprétation
:   Lara Barsacq<br/>

Aide à la dramaturgie
:  Clara Le Picard<br/>

Conseils artistiques
:   Gaël Santisteva<br/>

Costumes
:   Sofie Durnez<br/>

Création lumière
:   Kurt Lefevre<br/>

Musiques
:  Bauhaus, Claude Debussy et Maurice Ravel<br/>

Participation
:   Lydia Stock Brody et Nomi Stock Meskin<br/>

Production
:   asbl Gilbert & Stock

En coproduction avec Charleroi danse. Avec l’aide de la Fédération Wallonie-Bruxelles – service de la danse, de La Bellone, de la Ménagerie de Verre et du Théâtre la Balsamine.
{.production}

#### Entretenir une fougère

Je choisis la fougère la plus simple d'entretien, car très rustique, la fougère de Boston qui peut se placer à l'extérieur de mai à septembre.
{: .figure}

Je choisis un emplacement dans un bosquet sous des arbres.
Je soulève la terre avec une griffe.
J'apporte des gravillons et du compost dans les terres pauvres.
{: .figure}

<div class="visuels" markdown="true">
![lost-planter-fougere2](../../img/lost-planter-fougere2.svg){: .all}
![lost-planter-fougere2](../../img/lost-planter-fougere2-vecto.svg){: .vecto}
![lost-planter-fougere2](../../img/lost-planter-fougere2-couleur.svg){: .pantone-orange}
</div>

J’installe la fougère en terre.
Je ramène la terre existante pour combler le trou.
Je tasse légèrement.
Je remets la mousse existante au pied.
{: .figure}

Pour avoir de belles fougères, je pulvérise le feuillage avec de l'eau non calcaire.
Je disperse les feuilles sèches pour éloigner les esprits malfaisants, amener de bonnes ondes et chasser même la mélancolie.
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/lost-02.svg){: .all}
![alt](../../img/lost-02-vecto.svg){: .vecto}
![alt](../../img/lost-02-couleur.svg){: .pantone-orange}
</div>

</div>
