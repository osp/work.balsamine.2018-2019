<div class="article valhalla push" markdown="true">

#Valhalla <br>ou le crépuscule des dieux

##de Petri Dish / Anna Nilsson et Sara Lemaire

21.11 → 23.11
{: .dates}

Création cirque
{: .informations}

<div class="visuels" markdown="true">
![alt](../../img/valhalla-01.svg){: .all}
![alt](../../img/valhalla-01-vecto.svg){: .vecto}
![alt](../../img/valhalla-01-couleur.svg){: .pantone-vert}
</div>

Le pouvoir que l’on veut, celui que l’on prend.
{: .punchline .off}

Le crépuscule, un bateau pris dans la glace. Le naufrage d’un équipage qui sombre dans la solitude et la folie. Il s'y joue du cirque, du chant, de la danse et de la cornemuse. Les esprits s’échauffent. La mutinerie commence !
{: .figure .push}

Avec 
:   Joris Baltz, Viola Baroncelli, Laura Laboureur, Carlo Massari, Anna Nilsson et Jef Stevens<br/>

Concept et mise en scène
:  Anna Nilsson et Sara Lemaire<br/>

Création lumière
:   Philippe Baste<br/>

Technique
:   Tonin Bruneton, <br/>Cristian Gutierrez, Camille Rolovic

En coproduction avec Le Groupe des 20 Théâtres en Île-de-France, le Théâtre la Balsamine, Theater Op De Markt – Dommelhof et le Centre Culturel du Brabant Wallon.
Avec l’aide de la Fédération Wallonie-Bruxelles, service du cirque,  du  Kulturradet/Swedish Arts Council et du Konstnärsnämnden - the Swedish Arts Grants Committee.
Avec les soutiens du Circuscentrum, du Théâtre Marni, du Cirkus Cirkör, de Wolubilis, de Latitude 50, de l’Espace Catastrophe - Centre International de Création des Arts du Cirque et de Subtopia / Dansenshus.
{.production}

#### Tenir les voiles pendant la tempête

<div class="visuels" markdown="true">
![alt](../../img/valhalla-02.svg){: .all}
![alt](../../img/valhalla-02-vecto.svg){: .vecto}
![alt](../../img/valhalla-02-couleur.svg){: .pantone-vert}
</div>

Pour tenir les voiles pendant la tempête, je dois décider, immédiatement, entre deux options.<br/>
Dans la première option, dénommée option passive, je prend la cape.
Puis, basta.
{: .figure}

Dans la seconde option, dénommée option active, je vire de bord en laissant le foc à contre-bord. Et cela au près, sous foc et grand-voile.
{: .figure .push}

<div class="visuels" markdown="true">
![alt](../../img/valhalla-03.svg){: .all}
![alt](../../img/valhalla-03-vecto.svg){: .vecto}
![alt](../../img/valhalla-03-couleur.svg){: .pantone-vert}
</div>

<div class="visuels" markdown="true">
![alt](../../img/valhalla-04.svg){: .all}
![alt](../../img/valhalla-04-vecto.svg){: .vecto}
![alt](../../img/valhalla-04-couleur.svg){: .pantone-vert}
</div>

Je mets la barre au vent (sous le vent, si c'est une barre franche).
L'action du foc et celle de la grand-voile s'annulent.
Le voilier dérive à peu près en travers des vagues.
{: .figure}

</div>
