<div class="push">&nbsp;</div>
<div class="article carnets-peter" markdown="true">

#Les Carnets de Peter

##Théâtre du Tilleul

27.12 → 29.12<br/>
et<br/>
03.01 → 06.01
{: .dates}

Création<br/>
Spectacle d’ombres et de musique – dès 7 ans
{: .informations}

<div class="visuels" markdown="true">
![alt](../../img/carnets-00.svg){: .all}
![alt](../../img/carnets-00-vecto.svg){: .vecto}
![alt](../../img/carnets-00-couleur.svg){: .pantone-vert}
</div>

Peter Neumeyer et son petit héros Donald. Entre vie réelle et vie rêvée.
{: .punchline .off}

Peter, 87 ans, tente de se souvenir de son enfance : l’Allemagne, Munich 1936, les défilés militaires, les drapeaux, l’émigration en Amérique…
{: .figure}

Il se souvient, qu’un jour, seul dans la grande bibliothèque de son père, il décide d’inventer les étranges aventures d’un petit garçon nommé Donald, un petit garçon rêveur qui lui ressemble énormément sauf que...
{: .figure .push}

Avec
:   Carine Ermans, Carlo Ferrante, Sylvain Geoffray et Alain Gilbert<br/>

Conception du spectacle
:  Carine Ermans et Sylvain Geoffray<br/>

Conseil dramaturgique
:   Louis-Dominique Lavigne<br/>

Musique
:   Alain Gilbert<br/>

Mise en scène
:   Sabine Durand<br/>

Lumière
:  Mark Elst<br/>

Régie
:   Thomas Lescart<br/>

Scénographie
:   Pierre-François Limbosch et Alexandre Obolensky<br/>

Peintures
:   Alexandre et Eugénie Obolensky aidés de Malgorzata Dzierzawska et Tancrède de Ghellinck<br/>

Costumes
:   Silvia Hasenclever<br/>

Travail du mouvement
:   Isabelle Lamouline<br/>

Images animées
:  Patrick Theunen et Graphoui<br/>

Accessoires
:   Amalgames<br/>

Production 
:   Mark Elst<br/>

En coproduction avec le Théâtre la Balsamine avec la collaboration du Théâtre La montagne magique, du CDWEJ et de La Maison des Cultures et de la Cohésion Sociale de Molenbeek-Saint-Jean.
{.production}

Le Théâtre du Tilleul est en résidence artistique et administrative au Théâtre la Balsamine.
{.production}

#### Monter une bibliothèque

Je commence par le haut.
Je dispose un peu de colle à bois dans les perçages. J'y place ensuite les tourillons.
J'ajoute un point de colle sur le dessous de chacun des tourillons ainsi qu'une petite ligne entre eux.
J’insère la première planche verticale, celle d'un des deux côtés. Puis je fais la même opération avec la deuxième planche verticale.
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/carnets-01.svg){: .all}
![alt](../../img/carnets-01-vecto.svg){: .vecto}
![alt](../../img/carnets-01-couleur.svg){: .pantone-vert}
</div>

<div class="visuels" markdown="true">
![alt](../../img/carnets-02.svg){: .all}
![alt](../../img/carnets-02-vecto.svg){: .vecto}
![alt](../../img/carnets-02-couleur.svg){: .pantone-vert}
</div>

Je fixe ensuite les étagères en m’aidant des taquets prévus.
{: .figure}

Pour mettre en valeur ma bibliothèque, je fixe des bandeaux équipés de lampes LED, sur les étagères du haut.
{: .figure}

</div>
