<div class="article blanc xx" markdown="true">

# XX Time {: .push}

19.02 → 22.03
{: .dates}

<div class="visuels" markdown="true">
![alt](../../img/xx-time-01.svg){: .all}
![alt](../../img/xx-time-01-vecto.svg){: .vecto}
![alt](../../img/xx-time-01-blanc.svg){: .pantone-trou-blanc}
</div>

<div class="push"></div>

XX Time, temps privilégié dévolu à quelques artistes femmes interrogeant le féminin, la sexualité, les rapports dominants dominés. Corps souillé, corps poubelle, corps outil. Ce que les femmes font de leur corps, au cœur de leur sujet.
{: .informations}

Le corps de la femme peut-il enfin s’émanciper ?
{: .punchline .off}

<div class="push"></div>

### De la poésie, <br/>du sport, etc. {: .poesie}
##de Fanny Brouyaux et Sophie Guisset

19.02 → 20.02  
Création danse
{: .informations}

<div class="push"></div>

Du seuil de l’intime à un possible sésame vers l’universel, deux jeunes femmes courent et se livrent bataille, cherchant la voie de l’émancipation. Semé d'embûches, ce parcours physique devient poétique et dramatique.
{: .figure}

Avec
:   Fanny Brouyaux et Sophie Guisset<br/>

Dramaturgie
:   Claire Diez<br/>

Coach chorégraphique
:   Moya Michaël<br/>

Créateur Son
:   Christophe Rault<br/>

Créateur lumière
:   Raphaël Rubbens<br/>

Production déléguée
:   Mouvance a.s.b.l.<br/>

En coproduction avec Charleroi danse avec l’accompagnement du Grand Studio et les soutiens du Théâtre La coupole, de Pôle sud, du KVS et du Théâtre la Balsamine. 
{.production}

<div class="visuels" markdown="true">
![alt](../../img/xx-time-02.svg){: .all}
![alt](../../img/xx-time-02-vecto.svg){: .vecto}
![alt](../../img/xx-time-02-blanc.svg){: .pantone-trou-blanc}
</div>

#### Nouer ses lacets

Je fais mes lacets de basket avec un nœud standard.
Je tourne mon nœud dans le sens inverse des aiguilles d’une montre pour éviter que mon nœud ne soit parallèle à ma basket, ce qui n’est pas vraiment plaisant.
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/poesie-03.svg){: .all}
![alt](../../img/poesie-03-vecto.svg){: .vecto}
![alt](../../img/poesie-03-blanc.svg){: .pantone-trou-blanc}
</div>

<div class="visuels" markdown="true">
![alt](../../img/poesie-04.svg){: .all}
![alt](../../img/poesie-04-vecto.svg){: .vecto}
![alt](../../img/poesie-04-blanc.svg){: .pantone-trou-blanc}
</div>

<div class="visuels" markdown="true">
![alt](../../img/poesie-08.svg){: .all}
![alt](../../img/poesie-08-vecto.svg){: .vecto}
![alt](../../img/poesie-08-blanc.svg){: .pantone-trou-blanc}
</div>

<div class="push">&nbsp;</div>

Pour sortir de l’ordinaire, j'utilise le nœud dead knot, facile à faire, rapide et qui fait toujours son petit effet pour sortir des sentiers battus.
{: .figure}

<div class="push">&nbsp;</div>

<div class="visuels" markdown="true">
![poesie-05](../../img/poesie-05.svg){: .all}
![poesie-05](../../img/poesie-05-vecto.svg){: .vecto}
![poesie-05](../../img/poesie-05-blanc.svg){: .pantone-trou-blanc}
</div>

<div class="visuels" markdown="true">
![alt](../../img/poesie-06.svg){: .all}
![alt](../../img/poesie-06-vecto.svg){: .vecto}
![alt](../../img/poesie-06-blanc.svg){: .pantone-trou-blanc}
</div>

Inconvénient&nbsp;: avec cette technique, il m'est impossible de serrer à&nbsp;fond&nbsp;mes lacets.
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/poesie-07.svg){: .all}
![alt](../../img/poesie-07-vecto.svg){: .vecto}
![alt](../../img/poesie-07-blanc.svg){: .pantone-trou-blanc}
</div>

Petite minute info&thinsp;: c’est le&nbsp;nœud que font les fabricants de baskets sur&nbsp;les paires neuves.
{: .figure}

### Looking for the putes mecs
## de Diane Fourdrignier et Anne Thuot

1.03 → 2.03
{: .informations}

Reprise théâtre
{: .informations}

*Looking for the putes mecs*&nbsp; a reçu le Prix du Spectacle Vivant 2017 attribué par le Comité belge de la SACD.
{: .note}

<div class="visuels" markdown="true">
![looking-01](../../img/looking-01.svg){: .all}
![looking-01](../../img/looking-01-vecto.svg){: .vecto}
![looking-01](../../img/looking-01-blanc.svg){: .pantone-trou-blanc}
</div>

Deux femmes cherchent un péripatéticien, un «&nbsp;pute mec&nbsp;» et se confrontent à l’ignorance et aux clichés d’un désir formaté. Une interrogation de la place visible du désir féminin, une tentative d’ invention de nouveaux imaginaires érotiques.
{: .figure}

Avec
:   Diane Fourdrignier, Anne Thuot + Guests<br/>

Concept et réalisation
:   Diane Fourdrignier et Anne Thuot<br/>

Régie
:   Rémy Urbain<br/>

Photo
:   Pierre Debroux, Sara Sampelayo<br/>

Une production Fast ASBL. Avec le soutien du Théâtre la Balsamine, de l'Ancre – Théâtre Royal et du Théâtre des Doms.
{.production}

#### Faire un cunnilingus

<div class="visuels" markdown="true">
![alt](../../img/looking-02-b.svg){: .all}
![alt](../../img/looking-02-b-vecto.svg){: .vecto}
![alt](../../img/looking-02-b-blanc.svg){: .pantone-trou-blanc}
</div>

Je souffle sur les grandes lèvres de ma partenaire pour vérifier sa sensibilité, comme avant toute caresse manuelle ou buccale.
{: .figure}

Si notre relation ne peut impliquer d'échange de liquide ou de contact de muqueuse, je place la feuille d'une digue dentaire.
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/looking-03.svg){: .all}
![alt](../../img/looking-03-vecto.svg){: .vecto}
![alt](../../img/looking-03-blanc.svg){: .pantone-trou-blanc}
</div>

Puis j’ouvre sa fente de ma langue, de bas en haut, sans l’aide des mains.
Je répète ce mouvement plusieurs fois, d'abord lentement, en enfonçant ma langue à chaque fois davantage.
J’écarte délicatement les grandes lèvres avec les doigts, ce qui m’ouvre le vestibule, l’orifice du vagin et le clitoris.
{: .figure}

Tandis que ma langue fouille les petites lèvres et la fente vulvaire, je suis à l’écoute du moindre gémissement et de la plus petite contraction.
Je répète la caresse en variant l’intensité de la pression de ma langue.
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/looking-05.svg){: .all}
![alt](../../img/looking-05-vecto.svg){: .vecto}
![alt](../../img/looking-05-blanc.svg){: .pantone-trou-blanc}
</div>

Si je ne sens pas le doux-amer de la cyprine de la réaction chimique de ma partenaire, c'est qu'elle n'apprécie pas, j’arrête pour passer à autre chose ou en parler avec elle.
Dans le cas inverse, j’agace l’entrée du vagin de la pointe de ma langue avant d’approcher un doigt.
Là aussi je guette sa réactivité.
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/looking-08.svg){: .all}
![alt](../../img/looking-08-vecto.svg){: .vecto}
![alt](../../img/looking-08-blanc.svg){: .pantone-trou-blanc}
</div>

Je souffle sur le capuchon du clitoris pour vérifier sa sensibilité.
Je l’enroule du bout de la langue sans négliger pour autant le vestibule vulvaire, où, là aussi les réactions de ma partenaire me guident sur ses préférences. Certaines aiment qu’on aspire leur clitoris, qu’on le mordille, d’autres non.
À ce moment, si ma partenaire l’accepte, je la pénètre d’un doigt puis de deux si elle en fait la demande.
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/looking-06.svg){: .all}
![alt](../../img/looking-06-vecto.svg){: .vecto}
![alt](../../img/looking-06-blanc.svg){: .pantone-trou-blanc}
</div>

<div class="visuels" markdown="true">
![alt](../../img/looking-09.svg){: .all}
![alt](../../img/looking-09-vecto.svg){: .vecto}
![alt](../../img/looking-09-blanc.svg){: .pantone-trou-blanc}
</div>

<div class="push">&nbsp;</div>

<div class="visuels figure looking" markdown="true">
![looking-07](../../img/looking-07.svg){: .all}
![looking-07](../../img/looking-07-vecto.svg){: .vecto}
![looking-07](../../img/looking-07-blanc.svg){: .pantone-trou-blanc}
<p class="figure looking">Le clitoris en érection reste souvent décapu&shy;chonné, une de mes mains est libre pour caresser  les seins de ma partenaire.
Je suis en phase sur les trois zones érogènes d’importance. Quand l’excitation est à son comble, surtout ne pas s’arrêter. Une seconde d’inattention peut s’avérer fatale dans le processus, et tout serait alors à refaire.</p>
</div>

<div class="visuels" markdown="true">
![alt](../../img/looking-10.svg){: .all}
![alt](../../img/looking-10-vecto.svg){: .vecto}
![alt](../../img/looking-10-blanc.svg){: .pantone-trou-blanc}
</div>

### Cœur obèse {: .coeur}
## d'Amandine Laval

14.03 → 16.03
{: .informations}

Création - performance<br/>
+ de 18 ans
{: .informations}

<div class="visuels" markdown="true">
![img/coeur-00](../../img/coeur-00.svg){: .all}
![img/coeur-00](../../img/coeur-00-vecto.svg){: .vecto}
![img/coeur-00](../../img/coeur-00-blanc.svg){: .pantone-trou-blanc}
</div>

En ôtant sa culotte, en dézippant sa robe de soirée, en dégrafant sa lingerie sans en avoir l'air mais bien l'envie, en traquant le désir d'inconnus, en faisant jaillir leur excitation, en simulant le coup de foudre, le respect, l’amour véritable, compose-t-on finalement un spectacle comme un autre ? Une strip-teaseuse amateure, seule en scène, se décharge des fictions accumulées.
{: .figure .push}

Texte et interprétation
:   Amandine Laval<br/>

Musique
:   Pierre-Alexandre Lampert<br/>

Création lumière
:   Sébastien Corbière<br/>

Une production du Théâtre la Balsamine.
{.production}

#### Se dénuder

Je me mets pieds nus.
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/coeur-02.svg){: .all}
![alt](../../img/coeur-02-vecto.svg){: .vecto}
![alt](../../img/coeur-02-blanc.svg){: .pantone-trou-blanc}
</div>

Je marche.
J'enlève mon pull et je fais le tour de la pièce.
Je veille à aligner les pieds comme si je marchais sur une ligne.
J'utilise le bout de mes doigts.
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/coeur-03.svg){: .all}
![alt](../../img/coeur-03-vecto.svg){: .vecto}
![alt](../../img/coeur-03-blanc.svg){: .pantone-trou-blanc}
</div>

<div class="push">&nbsp;</div>

Je fixe quelque chose de mes yeux.
Je me positionne.
Je retire chaque pièce de vêtement séparément.
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/coeur-01.svg){: .all}
![alt](../../img/coeur-01-vecto.svg){: .vecto}
![alt](../../img/coeur-01-blanc.svg){: .pantone-trou-blanc}
</div>

Je me retourne.
Je m'appuie contre le mur.
Je vais lentement.
{: .figure}

### i-clit
## de Mercedes Dassy

20.03 → 22.03
{: .informations}

Reprise danse
{: .informations}

*i-clit*&nbsp; a reçu le prix Jo Dekmine 2018 <br/>décerné par <br/>le Théâtre <br/>des Doms.
{: .note}

<div class="visuels" markdown="true">
![iclit-side-00-2](../../img/iclit-side-00-2.svg){: .all}
![iclit-side-00-2](../../img/iclit-side-00-2-vecto.svg){: .vecto}
![iclit-side-00-2](../../img/iclit-side-00-2-blanc.svg){: .pantone-trou-blanc}
</div>

Une nouvelle vague du féminisme est née – ultra-connectée, ultra-sexuée et plus populaire. Mais face au pouvoir ambivalent de la pop culture, où et comment se placer dans ce combat en tant que jeune femme ?
*i-clit*&nbsp; traque ces moments de fragilité, où l’on oscille entre nouvelles formes d’oppression et affranchissement.
{: .figure .push .push-down}

Concept, chorégraphie, interprétation
:   Mercedes Dassy<br/>

Dramaturgie, regard extérieur
:   Sabine Cmelniski<br/>

Création lumière
:   Caroline Mathieu<br/>

Costumes, scénographie
:   Mercedes Dassy et Justine Denos<br/>

Création sonore
:   Clément Braive<br/>

Diffusion
:   Art Management Agency (AMA)<br/>

Production déléguée
:   Théâtre la Balsamine

En coproduction avec le Théâtre la Balsamine et Charleroi danse. Avec l’aide de la Fédération Wallonie-Bruxelles -  service de la danse, de la  S.A.C.D., du Théâtre Océan Nord, de l’Escaut, du B.A.M.P., de Project(ion) Room and Friends with benefits.
{.production}

Peut-on lire Simone de Beauvoir et rêver de tourner dans un clip de Beyoncé  ?
{: .punchline .off}

#### Twerker {: .no-push}

Je me mets en position accroupie.
Je ne suis pas trop près du sol, mais juste assez pour avoir un bon appui. Je rentre mes genoux à l'arrière de mes orteils pour éviter de me blesser.
Je place mes jambes bien écartées.
Je me baisse vers le sol.
Je tourne mes pieds vers l'extérieur, pour m'aider à rester en équilibre quand je commencerai à bouger.
Je pousse mes fesses vers l'arrière.
Je me place comme si j'allais m'asseoir sur une chaise.
Je garde les genoux pliés et je place mes mains sur mes hanches.
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/iclit-side-01.svg){: .all}
![alt](../../img/iclit-side-01-vecto.svg){: .vecto}
![alt](../../img/iclit-side-01-blanc.svg){: .pantone-trou-blanc}
</div>

Je garde le haut de mon corps bien droit et je regarde droit devant moi.
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/iclit-side-02.svg){: .all}
![alt](../../img/iclit-side-02-vecto.svg){: .vecto}
![alt](../../img/iclit-side-02-blanc.svg){: .pantone-trou-blanc}
</div>

En faisant ressortir mes fesses, je me penche en avant d'environ 45° et je fais basculer mon poids sur mes orteils.
Je remue mes fesses d'avant en arrière.
Je secoue mes fesses de haut en bas, en cambrant et redressant mon dos pour un meilleur résultat. Et je ne m’inquiète pas si j'ai des fesses plates, tout le monde peut réussir un bon twerk !
{: .figure}

Astuce : pendant que je m'entraîne, je répète mentalement cette citation de Fannie Sosa "Le twerk c’est avant tout une question de conscience qui arrive à des parties qui sont niées, cachées, censurées. C’est une danse qui met en mouvement le cul, le périnée, les organes génitaux: le ghetto du corps. Il s’agit de réveiller son “cul cosmique” l’agrandir, le rendre visible, le rendre beau."
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/iclit-side-03.svg){: .all}
![alt](../../img/iclit-side-03-vecto.svg){: .vecto}
![alt](../../img/iclit-side-03-blanc.svg){: .pantone-trou-blanc}
</div>

</div>
