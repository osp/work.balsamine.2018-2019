
<div class="start-info"></div>
<div class="pass" markdown=true>

#Le pass

Et si vous tentiez le pass… Un pass qui vous donne accès à 6 spectacles de la saison pour seulement 25€ !

Le Pass est nominatif et ne peut s’acheter que du 12 juin au 13 octobre 2018. Attention, le nombre de pass mis en vente est limité.
La réservation des places peut se faire à tout moment de la saison, sous réserve des places disponibles.

Intéressé ? Dites-le nous au 02 735 64 68  ou à reservation@balsamine.be , apportez-nous ensuite ce livret et nous vous attribuerons un code qui vous ouvrira les portes des spectacles suivants →

Nom
:    <br/>

Prénom
:    <br/>

Pass n°
:    <br/>

##Ørigine
3.10 → 5.10 et 9.10 → 13.10

##Où suis-je ? Qu’ai-je fait ?
2.11 → 3.11 et 5.11 → 10.11

##Valhalla ou le crépuscule des&nbsp;dieux
21.11 → 23.11

##Laboratoire Poison
29.01 → 02.02

##XX time
19.02 → 22.03<br/>
Au choix:<br/>
De la poésie, du sport etc./ Looking for the putes mecs / Cœur obèse / i-clit

##Ludum
02.04 → 05.04

</div>
