<div class="push">&nbsp;</div>

<div class="article poison" markdown="true">

#Laboratoire POISON

##d’Adeline Rosenstein

29.01 → 02.02
{: .dates}

Création
{: .informations}

Théâtre documentaire
{: .informations}

<div class="push">&nbsp;</div>

Des documents exhumés par le sociologue Jean-Michel Chaumont, dans le cadre de sa recherche sur les codes de conduite du combattant défait, sont le cœur de cette création théâtrale documentaire. À quel instant commence-t-on à ressentir la haine et à condamner l’autre ?
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/laboratoire-01.svg){: .all}
![alt](../../img/laboratoire-01-vecto.svg){: .vecto}
![alt](../../img/laboratoire-01-couleur.svg){: .pantone-orange}
</div>

Les héros purs ne sont-ils pas condamnés à jouer la comédie ? A quel stade, l’artifice devient-il synonyme de couardise ?
{: .punchline .off}

Avec
:   Brune Bazin, Olindo Bolzan, Ninon Borsei, Léa Drouet, Rémi Faure, Titouan Quitot, Martin Rouet et Thibaut Wenger<br/>

Texte et mise en scène
:   Adeline Rosenstein<br/>

Création son
:   Andrea Neumann<br/>

Scénographie
:   Yvonne Harder<br/>

Création lumière et direction technique
:   Caspar Langhoff<br/>

Costumes
:  Rita Belova<br/>

Production déléguée
:   Leïla Di Gregorio<br/>

Une production Little Big Horn en coproduction avec le Théâtre la Balsamine et la Coop asbl. Avec l’aide de la Fédération Wallonie-Bruxelles – Service du Théâtre et Service de la Promotion des lettres, de la Cocof et du kunstencentrum Buda. Avec le soutien de taxshelter.be, d’ING et du tax-shelter du gouvernement fédéral belge.
{.production}

#### SE FAIRE MAL OU TORTURER QUELQU’UN {: .push}

<div class="visuels" markdown="true">
![laboratoire-02](../../img/laboratoire-02.svg){: .all}
![laboratoire-02](../../img/laboratoire-02-vecto.svg){: .vecto}
![laboratoire-02](../../img/laboratoire-02-couleur.svg){: .pantone-orange}
</div>

Je m’ouvre les veines ou alors j’applique plutôt un fer à repasser sur le torse nu d’une personne attachée les bras en l’air.
J’aligne soigneusement le sens du fer à repasser la pointe vers le haut.
J’utilise la fonction vapeur pour ébouillanter la personne.
Attention : je prends garde d’utiliser de l’eau distillée pour ne pas provoquer de dépôt de calcaire dans le compartiment.
{: .figure}

</div>
