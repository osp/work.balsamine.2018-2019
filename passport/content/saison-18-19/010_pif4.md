<div class="push"></div>
<div class="article blanc pif4" markdown="true">

#PIF 4 – Pauvre et Insurgé Festival

11.06 → 15.06
{: .dates}

Quatrième édition du festival ouvert à toutes les révoltes.
{: .informations}

Programmation en cours
{: .informations}

![alt](../../img/pif-00.svg)

#### Construire une barricade

![alt](../../img/pif-01.svg)
Je marque les pièces de deux par quatre à 3 pieds de longs intervalles et je les coupe à la scie circulaire. Je coupe six morceaux pour chaque barricade en bois que je construis.
Je centre une pièce sur sa longueur au-dessus des deux autres posées sur leur largeur, ce qui me donne une structure en T qui servira au sommet des barricades.
Je cloue les uns aux autres avec autant de clous que j'en juge nécessaire (voir double page précédente).
Je coupe un angle de 15° sur les bords supérieur et inférieur de toutes les pièces de la jambe.
Je marque une pièce de pieds en long morceau de matériau à deux par quatre le long de la largeur de 4 pouces à partir du coin de la matière et je trace une ligne avec le carré du charpentier du coin le plus extérieur jusqu'à 15°.
Je fais la même marque sur l'extrémité opposée de la jambe.
{: .figure}

![alt](../../img/pif-02.svg)
Je coupe quatre morceaux de matériau pour chaque barricade.
Je place le haut de sorte à ce que le T soit inversé, avec la base de la section de 4 pouces de large et l'autre pièce montée au-dessus.
Je défini une jambe de 3 pouces à partir du bord extérieur de la partie supérieure et je la règle de sorte que l'angle de 15° corresponde à plat avec le haut et l'angle des jambes vers l'extérieur avec l'extrémité opposée de la jambe à plat contre le sol.
Je cloue en place et je répète le processus pour les trois autres pieds sur la barricade.
{: .figure}

</div>
