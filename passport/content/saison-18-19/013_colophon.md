<div class="start-info"></div>
<div class="info-pratique colophon" markdown=true>

#Colophon

Dessins et graphisme
:   Open Source Publishing<br/>

Outils et programmes
:   html2print, Inkscape, Gimp, git, gitlab, etherpad, GNU/Linux<br/>

Typographies
:   Ume stroke stroke condensed, Ume stroke stroke expended<br/>

Fichiers de mise en page disponibles sous licence art libre sur http://osp.kitchen /work/balsamine.2018-2019/
{: .production}

Impression
:   Imprimerie Gillis, Bruxelles<br/>

Éditeur responsable
:   Monica Gomes, Avenue Félix Marchal, 1 - 1030 Bruxelles<br/>
