<div class="push">&nbsp;</div>
<div class="push">&nbsp;</div>

<div class="article ou-suis-je" markdown="true">

#Où suis-je ? Qu’ai-je fait ?

##de Pauline d’Ollone 

2.11 → 3.11<br/>
et <br/>
5.11 → 10.11
{: .dates}

Création théâtre
{: .informations}

<div class="visuels" markdown="true">
![ou-suis-je-01](../../img/ou-suis-je-01.svg){: .all}
![ou-suis-je-01](../../img/ou-suis-je-01-vecto.svg){: .vecto}
![ou-suis-je-01](../../img/ou-suis-je-01-couleur.svg){: .pantone-orange}
</div>

Aiguisons notre esprit critique avec une lucidité rieuse !
{: .punchline .off}

<div class="push">&nbsp;</div>

Les points communs entre&nbsp;Roméo et Juliette, le&nbsp;Journal de Goebbels ou&nbsp;encore un site anti-arnaque&nbsp;?  Des person&shy;nages qui cherchent, à n’importe quel prix, un sens à leur destinée. Trois histoires, trois formes d’asservissement volontaire.
{: .figure}

Avec 
:   Marie Bos, Pierange Buondelmonte, Héloïse Jadoul <br/>et Jérémie Siska<br/>

Texte et mise en scène<br/>
:   Pauline d’Ollone<br/>

Scénographie 
:   Pierange Buondelmonte et Guillaume Fromentin<br/>

Création lumière
:   Renaud Ceulemans<br/>

Assistanat à la mise en scène
:   Sarah Messens<br/>

Construction du décor<br/>
:   Didier Rodot<br/>

Production déléguée
:   Théâtre la Balsamine<br/>

En coproduction avec le Théâtre la Balsamine, L’Ancre–Théâtre Royal, La Coop asbl et Shelter Prod. Avec les soutiens du Centre des Arts Scéniques, de la Fédération Wallonie-Bruxelles – Aide aux projets théâtraux, de taxshelter.be, d’ING et du tax-shelter du gouvernement fédéral belge.
{.production}

#### Le piège à mouches

<div class="visuels" markdown="true">
![alt](../../img/ou-suis-je-02.svg){: .all}
![alt](../../img/ou-suis-je-02-vecto.svg){: .vecto}
![alt](../../img/ou-suis-je-02-couleur.svg){: .pantone-orange}
</div>

Je confectionne un piège à mouches avec de la levure de boulanger (appât idéal contre les mouches et les moustiques).
{: .figure .push}

<div class="visuels" markdown="true">
![ou-suis-je-03](../../img/ou-suis-je-03.svg){: .all}
![ou-suis-je-03](../../img/ou-suis-je-03-vecto.svg){: .vecto}
![ou-suis-je-03](../../img/ou-suis-je-03-couleur.svg){: .pantone-orange}
</div>

J’enleve le bouchon.  
Je coupe la bouteille en deux à l’aide d’un cutter. Je fais bien attention de ne pas me couper, ça serait trop bête de se blesser en voulant tuer des bêtes&thinsp;!             
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/ou-suis-je-04.svg){: .all}
![alt](../../img/ou-suis-je-04-vecto.svg){: .vecto}
![alt](../../img/ou-suis-je-04-couleur.svg){: .pantone-orange}
</div>

Je verse 200&nbsp;ml d’eau tiède et 50 g de sucre roux, ou à défaut de sucre blanc, dans le fond de la bouteille et je mélange bien pour faire dissoudre le sucre.
{: .figure}

<div class="visuels figure demi" markdown="true">
![ou-suis-je-05](../../img/ou-suis-je-05.svg){: .all}
![ou-suis-je-05](../../img/ou-suis-je-05-vecto.svg){: .vecto}
![ou-suis-je-05](../../img/ou-suis-je-05-couleur.svg){: .pantone-orange}
<p class="figure demi down">Je laisse refroidir. Je saupoudre de 1 g de levure de boulanger.</p>
</div>

<div class="visuels figure demi right" markdown="true">
![ou-suis-je-06-2](../../img/ou-suis-je-06-2.svg){: .all}
![ou-suis-je-06-2](../../img/ou-suis-je-06-2-vecto.svg){: .vecto}
![ou-suis-je-06-2](../../img/ou-suis-je-06-2-couleur.svg){: .pantone-orange}
</div>

J’emboîte la partie haute de la bouteille, goulot en bas pour former un entonnoir, dans la partie basse.
{: .figure}

Au bout de 3 à 4 jours, le mélange commencera à fermenter et à dégager du gaz carbonique qui attire les mouches (et les moustiques). La fermentation sera à son maximum au bout de 15 jours. Le piège reste efficace pendant 2 à 3 semaines, ensuite je renouvelle l’appât.
{: .figure}

Astuce : si je n’ai pas de levure de boulanger, j’utilise : un mélange de bière et de sucre, de l'eau additionnée d'une grande quantité de sirop de fruit, de préférence rouge ou jaune, ces couleurs attirant davantage les mouches, un reste de vin ou simplement de l'eau très sucrée.
{: .figure}

Attention : on n’attrape pas les mouches avec du vinaigre.
{: .figure .push}

</div>
<div class="push"></div>
