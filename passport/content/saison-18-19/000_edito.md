<div class="edito" markdown=true>

#Saison 18-19
#Économe

Une saison qui fait l’économie de tout sauf de la créativité <br/>et du partage qui peuvent se développer sans limites.<br>
Une sobriété heureuse, colorée, inclusive, rationnelle, minimale et performante à la fois. Une saison en phase avec notre temps de la décroissance, de l’économie circulaire, <br/>du « smart », du modulable.<br/>

En ces temps où le développement durable n’est plus une mode mais une indispensable nécessité, il est temps de questionner l’ensemble de nos choix. Qu’est-ce qui est fondamental dans notre quotidien et comment plaçons-nous dès lors notre énergie sur ce qui émerge …<br>
Une saison à éplucher sans modérations pour avoir la patate !
{: .txtedito1}

Monica Gomes, direction générale et artistique de la Balsamine.
{: .footer}


</div>
