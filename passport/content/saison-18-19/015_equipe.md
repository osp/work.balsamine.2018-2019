<div class="start-info"></div>
<div class="push">&nbsp;</div>

<div class="info-pratique equipe" markdown=true>

#Équipe

Direction générale et artistique
:   Monica Gomes<br/>

Direction financière et administrative
:   Morgan Brunéa<br/>

Coordination générale, communication et accueil compagnies
:   Fanny Arvieu<br/>

Presse, promotion et relations publiques
:   Irène Polimeridis<br/>

Comédienne et romaniste - Médiation écoles et associations
:   Noemi Tiberghien<br/>

Metteur en scène et auteur - Artiste associée
:   Martine Wijckaert<br/>
<br/>

Résidence artistique et administrative
:   Théâtre du Tilleul<br/>

Direction technique
:   Jef Philips<br/>

Régisseur
:   Rémy Urbain<br/>

Régisseur stagiaire
:   Brice Agnès<br/>

Responsable bar
:   Franck Crabbé<br/>

Photographe de plateau associé
:   Hichem Dahes<br/>

Designers graphique associés
:   Open Source Publishing
