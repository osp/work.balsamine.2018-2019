<div class="push"></div>
<div class="article origine" markdown="true">


#ØRIGINE

##de Silvio Palomo/Le comité des fêtes 

3.10 → 5.10<br/>
et <br/>
9.10 → 13.10
{: .dates}

Création théâtre
{: .informations}

<div class="push"></div>

Les protagonistes de cette aventure sont un homme et une femme. Un pique-nique s’organise. Un ours et un pingouin s’invitent. Du coup, leur évènement s’en trouve bouleversé.
{: .figure}

<div class="visuels push-down" markdown="true">
![origine-1](../../img/origine-01.svg){: .all}
![origine-1](../../img/origine-01-vecto.svg){: .vecto}
![origine-1](../../img/origine-01-couleur.svg){: .pantone-vert .push-down}
</div>

<div class="push"></div>

Avec 
:   Léonard Cornevin, Aurélien Dubreuil-Lachaud, Manon Joannotéguy et Nicole Stankiewicz<br/>

Texte et mise en scène
:   Silvio Palomo<br/>

Scénographie 
:   Itzel Palomo<br/>

Création lumière
:   Léonard Cornevin<br/>

Production déléguée
:   Théâtre la Balsamine<br/>

En coproduction avec le Théâtre la Balsamine, La Coop asbl et Shelter Prod. Avec les soutiens de la Fédération Wallonie-Bruxelles – Aide aux projets théâtraux, de taxshelter.be, d’ING et du tax-shelter du gouvernement fédéral belge.
{.production}

#### Déplier une table pliante

<div class="visuels" markdown="true">
![origine-2](../../img/origine-02.svg){: .all}
![origine-2](../../img/origine-02-vecto.svg){: .vecto}
![origine-2](../../img/origine-02-couleur.svg){: .pantone-vert}
</div>

Pour bien déplier une table pliante, je commence par poser le boîtier qui la renferme sur une surface plane et dégagée.
Je déverrouille le loquet du boîtier, sous la poignée arrondie.
{: .figure}

<div class="visuels" markdown="true">
![origine-3](../../img/origine-03.svg){: .all}
![origine-3](../../img/origine-03-vecto.svg){: .vecto}
![origine-3](../../img/origine-03-couleur.svg){: .pantone-vert}
</div>

J’ouvre le boîtier entièrement. Les pieds et les bancs se présentent à moi.
Je glisse mes doigts sous le banc de gauche et je le déplie jusqu’à la butée.
{: .figure}

<div class="push"></div>

<div class="visuels figure demi" markdown="true">
![origine-4](../../img/origine-04.svg){: .all}
![origine-4](../../img/origine-04-vecto.svg){: .vecto}
![origine-4](../../img/origine-04-couleur.svg){: .pantone-vert}
<p class="right figure">Attention, les bords en plastique, mal ébarbés par un usinage dans des conditions navrantes, sont souvent coupants.</p>
</div>

Je fais de même avec l’autre banc puis je déplie les pieds en prenant garde de ne pas forcer sur les charnières en plastique, fragiles malgré leur apparente robustesse calculée.
{: .figure}

<div class="visuels" markdown="true">
![origine-5](../../img/origine-05.svg){: .all}
![origine-5](../../img/origine-05-vecto.svg){: .vecto}
![origine-5](../../img/origine-05-couleur.svg){: .pantone-vert}
</div>

<div class="visuels" markdown="true">
![origine-6](../../img/origine-06.svg){: .all}
![origine-6](../../img/origine-06-vecto.svg){: .vecto}
![origine-6](../../img/origine-06-couleur.svg){: .pantone-vert}
</div>

Je bloque chacune des paires de pieds à l’aide du cliquet ad hoc.
{: .figure}

</div>
