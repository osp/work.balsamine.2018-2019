<div class="start-info"></div>
<div class="info-pratique partenaires" markdown=true>

#Partenaires

![label-united-stages-txt](../../img/logos/label-united-stages-txt.svg)
{: .figure .demi}

La Balsamine est signataire de la charte United Stages. Une «&nbsp;United stage&nbsp;» est une scène qui marque son engagement vers une politique migratoire basée sur l’hospitalité, le respect des droits humains et les valeurs de solidarité. Les engagements pris par les différentes «&nbsp;United stages&nbsp;» sont scellés par la signature d’une charte commune à tous les partenaires.
{: .production}

La Balsamine est subventionnée par la Fédération Wallonie-Bruxelles et fait partie du réseau des Scènes chorégraphiques de la Commission Communautaire française de la Région de Bruxelles-Capitale. La Balsamine reçoit le soutien de Wallonie-Bruxelles Théâtre/Danse et de Wallonie-Bruxelles International.
{: .production}

![logos-all](../../img/logos/logos-all.svg)
{: .figure}
