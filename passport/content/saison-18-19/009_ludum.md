<div class="push">&nbsp;</div>

<div class="article ludum" markdown="true">

#Ludum

## d’Anton Lachky

02.04 → 05.04
{: .dates}

Création danse
{: .informations}

<div class="visuels" markdown="true">
![ludum-00](../../img/ludum-00.svg){: .all}
![ludum-00](../../img/ludum-00-vecto.svg){: .vecto}
![ludum-00](../../img/ludum-00-couleur.svg){: .pantone-vert}
</div>

Les habitants de Ludum présentent tous une parti&shy;cularité, une bizarrerie&thinsp;: ils sont libres d'être et de faire ce qu'ils veulent. À Ludum,
société ultra intégrante où les lubies des uns et des autres coexistent en parfaite harmonie, le jeu est&nbsp;loi.
{: .figure .push-down}

Avec
:   Dennis Alamanos, Robert Anderson, Guilhem Chantir, Ana Karenina Lambrechts et Ioulia Zacharaki<br/>

Concept et chorégraphie
:   Anton Lachky<br/>

Une production d’Anton Lachky Company en coproduction avec le Théâtre la Balsamine et Charleroi danse.
{.production}

Dystopie ou utopie ?
{: .punchline .off}

#### Enfoncer un clou

Je dois d’abord lui choisir une taille adéquate. Le débutant choisit presque toujours un clou trop <br/>gros, qui risque de <br/>fendre le bois.
Un petit <br/>clou bien mis en place tient mieux qu'un gros clou mal planté.
Je tiens le marteau par l'extrémité du manche, et non près de la tête.
{: .figure}

<div class="visuels" markdown="true">
![ludum-01](../../img/ludum-01.svg){: .all}
![ludum-01](../../img/ludum-01-vecto.svg){: .vecto}
![ludum-01](../../img/ludum-01-couleur.svg){: .pantone-vert}
</div>

<div class="visuels" markdown="true">
![alt](../../img/ludum-02.svg){: .all}
![alt](../../img/ludum-02-vecto.svg){: .vecto}
![alt](../../img/ludum-02-couleur.svg){: .pantone-vert}
</div>

Pour maintenir momentanément deux pièces de bois (fermer une caisse par exemple) j'enfonce le clou perpendiculairement. Il sera ainsi plus facile à retirer.
Pour un clouage définitif, j'enfonce le clou de biais, dans le sens opposé à la traction.
{: .figure}

Je ne cloue pas trop près du bord des pièces.
Pour clouer, je pose le clou en place, je donne d'abord de légers coups en maintenant le clou, puis je le lâche et je l'enfonce franchement par quelques coups forts et répétés.
{: .figure}

<div class="visuels" markdown="true">
![alt](../../img/ludum-03.svg){: .all}
![alt](../../img/ludum-03-vecto.svg){: .vecto}
![alt](../../img/ludum-03-couleur.svg){: .pantone-vert}
</div>

Pour éviter de fendre le bois, s'il est tendre ou mince, je perce un avant-trou. Ou j'épointe le clou d'un petit coup de marteau : ainsi le clou écrase les fibres au lieu de les écarter.
Je cloue toujours sur un support bien plan, rigide et résistant, afin que les pièces à clouer ne vibrent pas.
{: .figure}

</div>
